﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace luisbraga.Models{

    public class ClientePedido{

        public int CnpjId {get; set;}
	public Cliente Cliente {get; set;}
	
	public int PedidoId {get; set;}
	public Pedido Pedido {get; set;}

    }

}
