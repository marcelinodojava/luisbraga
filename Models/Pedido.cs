﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace luisbraga.Models{

    public class Pedido{

        public int PedidoId {get; set;}
        
        public float ValorPedido {get; set;}

        public int Quantidade {get; set;}

        public float Frete {get; set;}

        public DateTime DataPedido {get; set;}

	public ICollection <PedidoProduto> PedidoProdutos {get; set;}

    }

}
