﻿using luisbraga.Models;
using Microsoft.EntityFrameworkCore;

namespace TarefaApi.Model
{
  public class ApiDbContext : DbContext
  {
    public ApiDbContext(DbContextOptions<ApiDbContext> options)
        : base(options)
    { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
	// ClienteFornecedor
      modelBuilder.Entity<ClienteFornecedor>()
          .HasKey(tr => new { tr.CnpjId, tr.FCnpjId });
      modelBuilder.Entity<ClienteFornecedor>()
          .HasOne(tr => tr.Cliente)
          .WithMany(t => t.ClienteFornecedores)
          .HasForeignKey(tr => tr.CnpjId);
      modelBuilder.Entity<ClienteFornecedor>()
          .HasOne(tr => tr.Fornecedor)
          .WithMany(r => r.ClienteFornecedores)
          .HasForeignKey(tr => tr.FCnpjId);
	
	// ClientePedido
      modelBuilder.Entity<ClientePedido>()
          .HasKey(tr => new { tr.CnpjId, tr.PedidoId });
      modelBuilder.Entity<ClientePedido>()
          .HasOne(tr => tr.Cliente)
          .WithMany(t => t.ClientePedidos)
          .HasForeignKey(tr => tr.CnpjId);
      modelBuilder.Entity<ClientePedido>()
          .HasOne(tr => tr.Pedido)
          .WithMany(r => r.ClientePedidos)
          .HasForeignKey(tr => tr.PedidoId);

	// ClienteProduto
      modelBuilder.Entity<ClienteProduto>()
          .HasKey(tr => new { tr.CnpjId, tr.ProdutoId });
      modelBuilder.Entity<ClienteProduto>()
          .HasOne(tr => tr.Cliente)
          .WithMany(t => t.ClienteProdutos)
          .HasForeignKey(tr => tr.CnpjId);
      modelBuilder.Entity<ClienteProduto>()
          .HasOne(tr => tr.Produto)
          .WithMany(r => r.ClienteProdutos)
          .HasForeignKey(tr => tr.ProdutoId);

	// FornecedorPedido
      modelBuilder.Entity<FornecedorPedido>()
          .HasKey(tr => new { tr.FCnpjId, tr.PedidoId });
      modelBuilder.Entity<FornecedorPedido>()
          .HasOne(tr => tr.Fornecedor)
          .WithMany(t => t.FornecedorPedidos)
          .HasForeignKey(tr => tr.FCnpjId);
      modelBuilder.Entity<FornecedorPedido>()
          .HasOne(tr => tr.Pedido)
          .WithMany(r => r.FornecedorPedidos)
          .HasForeignKey(tr => tr.PedidoId);

	// FornecedorProduto
    modelBuilder.Entity<FornecedorProduto>()
          .HasKey(tr => new { tr.FCnpjId, tr.ProdutoId });
      modelBuilder.Entity<FornecedorProduto>()
          .HasOne(tr => tr.Fornecedor)
          .WithMany(t => t.FornecedorProdutos)
          .HasForeignKey(tr => tr.FCnpjId);
      modelBuilder.Entity<FornecedorProduto>()
          .HasOne(tr => tr.Produto)
          .WithMany(r => r.FornecedorProdutos)
          .HasForeignKey(tr => tr.ProdutoId);

	// PedidoProduto
    modelBuilder.Entity<PedidoProduto>()
          .HasKey(tr => new { tr.PedidoId, tr.ProdutoId });
      modelBuilder.Entity<PedidoProduto>()
          .HasOne(tr => tr.Pedido)
          .WithMany(t => t.PedidoProdutos)
          .HasForeignKey(tr => tr.PedidoId);
      modelBuilder.Entity<PedidoProduto>()
          .HasOne(tr => tr.Produto)
          .WithMany(r => r.PedidoProdutos)
          .HasForeignKey(tr => tr.ProdutoId);

    }

// arrumar

    public DbSet<Pedido> Pedidos { get; set; }
    public DbSet<Produto> Produtos { get; set; }
    public DbSet<Cliente> Clientes { get; set; }
    public DbSet<Fornecedor> Fornecedores { get; set; }
  }
}
