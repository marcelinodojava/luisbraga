﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace luisbraga.Models{

    public class Cliente
    {
        public ICollection<ClienteProduto> clienteProdutos;

        public int CnpjId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public int Telefone { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public int CEP { get; set; }
        public string Bairro { get; set; }
        public string Rua { get; set; }
        public int Numero { get; set; } 
        public String Complemento { get; set; }

        public ICollection<ClienteProduto> ClienteProdutos { get;set; }
        public ICollection<ClientePedido> ClientePedidos { get; set; }
        public ICollection<ClienteFornecedor> ClienteFornecedores { get; set; }

    }

}
